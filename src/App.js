
import './App.css';
import React from "react";
import Unity, { UnityContext } from "react-unity-webgl";

const unityContext = new UnityContext({
  loaderUrl: "unity_build/build.loader.js",
  dataUrl: "unity_build/build.data",
  frameworkUrl: "unity_build/build.framework.js",
  codeUrl: "unity_build/build.wasm",
});

function App() {
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [progression, setProgression] = React.useState(0);

  React.useEffect(() => {
    unityContext.on("canvas", handleOnUnityCanvas);
    unityContext.on("progress", handleOnUnityProgress);
    unityContext.on("loaded", handleOnUnityLoaded);
    return function () {
      unityContext.removeAllEventListeners();
    };
  }, []);

  function handleOnUnityCanvas(canvas) {
    canvas.setAttribute("role", "unityCanvas");
  }

   function handleOnUnityProgress(progression) {
    setProgression(progression);
  }

  // Built-in event invoked when the Unity app is loaded.
  function handleOnUnityLoaded() {
    setIsLoaded(true);
  }

  function handleOnClickFullscreen() {
    unityContext.setFullscreen(true);
  }

  return (
    <div className="wrapper">

        <h1>React Unity WebGL Test</h1>   
        <button onClick={() => handleOnClickFullscreen()}>setFullscreen</button>     
            <div className="unity-container">
              {/* The loading screen will be displayed here. */}
              {isLoaded === false && (
                <div className="loading-overlay">
                  <div className="progress-bar">
                    <div
                      className="progress-bar-fill"
                      style={{ width: progression * 100 + "%" }}
                    />
                  </div>
                </div>
              )}
              {/* The Unity app will be rendered here. */}
              <Unity className="unity-canvas" unityContext={unityContext} />
            </div>
      </div>
  );
}

export default App;
